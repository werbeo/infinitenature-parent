Infinitenature - parent pom
============================
[![Maven Central](https://img.shields.io/maven-central/v/org.infinitenature/infinitenature-parent.svg)]() 

Release
-------

`./mvnw clean`

`./mvnw release:clean -Prelease`

`./mvnw release:prepare -Prelease`

`./mvnw release:perform -Prelease`
